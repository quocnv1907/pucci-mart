$(function () {
    $.validator.setDefaults({
        submitHandler: function () {
            // onBtnCreatePetClick();
        }
    });

    $('#modal-add-pet').validate({
        rules: {
            name: {
                required: true
            },
            type: {
                required: true,
            },
            url: {
                required: true,
                url: true 
            },
            price: {
                required: true,
                min: 1
            },
            description: {
                required: true,
                minlength: 10
            },
            discount: {
                required: true,
                min: 0, 
                max: 100
            },
            promotion_price: {
                required: true,
                min: 0
            }
        },
        messages: {
            name: {
                required: "Vui lòng nhập tên vật nuôi",
            },
            type: {
                required: "Vui lòng chọn loài vật nuôi",
            },
            url: {
                required: "Vui lòng nhập đường dẫn ảnh",
                url: "Hãy nhập link hợp lệ"
            },
            price: {
                required: "Vui lòng nhập giá",
                min: "Giá phải lớn hơn 0"
            },
            description: {
                required: "Mô tả không được để trống",
                minlength: "Mô tả có ít nhất 10 kí tự"
            },
            discount: {
                required: "Hãy nhập % giảm giá",
                min: "Giảm giá phải lớn hơn hoặc bằng 0%",
                max: "Giảm giá không được vượt quá 100%"
            },
            promotion_price: {
                required: "Hãy nhập giá khuyến mãi",
                min: "Giá khuyến mãi không được là số âm ",
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    // Reset form and validation on modal close
    $('#modal-add-pet').on('hidden.bs.modal', function () {
         //Clear form inputs
         $('#modal-add-pet')[0].reset();
        // Remove error classes and messages
        $('#modal-add-pet').find('.is-invalid').removeClass('is-invalid');
        $('#modal-add-pet').find('.invalid-feedback').removeClass('.invalid-feedback');
    });
});


//apply for form edit
$(function () {
    $.validator.setDefaults({
        submitHandler: function () {
            onBtnUpdateClick();
        }
    });

    $('#modal-edit-pet').validate({
        rules: {
            name: {
                required: true
            },
            type: {
                required: true,
            },
            url: {
                required: true,
                url: true 
            },
            price: {
                required: true,
                min: 1
            },
            description: {
                required: true,
                minlength: 10
            },
            discount: {
                required: true,
                min: 0, 
                max: 100
            },
            promotion_price: {
                required: true,
                min: 0
            }
        },
        messages: {
            name: {
                required: "Vui lòng nhập tên vật nuôi",
            },
            type: {
                required: "Vui lòng chọn loài vật nuôi",
            },
            url: {
                required: "Vui lòng nhập đường dẫn ảnh",
                url: "Hãy nhập link hợp lệ"
            },
            price: {
                required: "Vui lòng nhập giá",
                min: "Giá phải lớn hơn 0"
            },
            description: {
                required: "Mô tả không được để trống",
                minlength: "Mô tả có ít nhất 10 kí tự"
            },
            discount: {
                required: "Hãy nhập % giảm giá",
                min: "Giảm giá phải lớn hơn hoặc bằng 0%",
                max: "Giảm giá không được vượt quá 100%"
            },
            promotion_price: {
                required: "Hãy nhập giá khuyến mãi",
                min: "Giá khuyến mãi không được là số âm ",
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    // Reset form and validation on modal close
    $('#modal-edit-pet').on('hidden.bs.modal', function () {
         //Clear form inputs
         $('#modal-edit-pet')[0].reset();
        // Remove error classes and messages
        $('#modal-edit-pet').find('.is-invalid').removeClass('is-invalid');
        $('#modal-edit-pet').find('.invalid-feedback').removeClass('.invalid-feedback');
    });
});


//apply for form filter
$(function () {
    $.validator.setDefaults({
        submitHandler: function () {
            filteredPet();
        }
    });

    $('#form-filter').validate({
        rules: {
            max_price: {
                required: true,
                min: 0
            },
            min_price: {
                required: true,
                min: 0
            }
        },
        messages: {

            max_price: {
                required: "Hãy nhập giá cao nhất",
                min: "Giá phải lớn hơn 0"
            },
            min_price: {
                required: "Hãy nhập giá thấp nhất",
                min: "Giá phải lớn hơn 0"
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    // Reset form and validation on modal close
    $('#form-filter').on('hidden.bs.modal', function () {
         //Clear form inputs
         $('#form-filter')[0].reset();
        // Remove error classes and messages
        $('#form-filter').find('.is-invalid').removeClass('is-invalid');
        $('#form-filter').find('.invalid-feedback').removeClass('.invalid-feedback');
    });
});










