### 1. Mô tả dự án
trang web mua bán và Quản lý danh sách vật nuôi

### 2. Giải thích code
- các hàm gọi API lấy danh sách và chi tiết vật nuôi, thực hiện chức năng CRUD: 
 callApiGetListPet, callApiAdd, callApiUpdate, callApiDelete, callApiGetPetById
- hàm onBtnCreatePetClick() được gọi thực hiện chức năng thêm mới 1 vật nuôi, khi nhấn nút Thêm trên modal
- hàm onBtnUpdateClick() được gọi khi thực hiện cập nhật thông tin vật nuôi
- hàm checkInputEdit, checkInputAdd kiểm tra dữ liệu nhập vào trên form Chỉnh sửa và form thêm mới
- hàm loadListPetOnTable thực hiện tải dữ liệu từ API lên data table
- hàm getPetIdFromButton thực hiện lấy id của vật nuôi từ các hàng tương ứng trong bảng
- hàm getInputAdd, getInputEdit lấy dữ liệu nhập vào từ các form Thêm và form sửa
- hàm showPetDetailToModal thực hiện load dữ liệu chi tiết vật nuôi lên form
- các hàm handleUpdateSuccess, handleAddSuccess, handleDeleteSuccess xử lý hiển thị thông báo thêm, sửa, xóa thành công
- hàm onPriceFilterClick(min, max) thực hiện lọc danh sách pet theo giá cao nhất và thấp nhất
- hàm callApiFilter gọi API lấy danh sách pet có bộ lọc
- hàm displayPetOnWeb thực hiện hiển thị danh sách pet lên web

### 3. các chức năng
- tải danh sách vật nuôi từ API lên web:
![alt text](<images/tai danh sach pet len web.png>)

- tải danh sách vật nuôi từ API lên data table:
![alt text](<images/tai danh sach len table.png>)

- lọc danh sách theo giá cao nhất và thấp nhất
![alt text](<images/loc danh sach.png>)

- thêm: 
![alt text](images/them.png)

- sửa :
![alt text](images/sua.png)

- xóa:
![alt text](images/xoa.png)

### 4. Công nghệ sử dụng:
html, css, bootstrap 4, ajax, jquery, Data table

