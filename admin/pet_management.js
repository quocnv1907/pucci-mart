
/***VÙNG KHAI BÁO HẰNG SỐ, CÁC BIẾN TOÀN CỤC */
var gBASE_URL = "https://pucci-mart.onrender.com/api";
const gCONTENT_TYPE = "application/json";

var gPetId = 0;

/***Khởi tạo data table, các cột giá trị null cần RENDER lại */
var gDataTable = $("#table-pets").DataTable({
    columns: [
        { data: "id" },
        { data: "name" },
        { data: "price" },
        { data: "type" },
        { data: "imageUrl" },
        { data: "discount" },
        { data: "description" },
        { data: "action" }
    ],
    columnDefs: [
        {
            targets: 7, //vị trí cột action
            defaultContent: `
            <div class="row d-flex justify-content-center">
                <i class="fa-solid fa-pen-to-square btn-edit mx-2"></i>
                <i class="fa-solid fa-trash btn-delete"></i>
            </div>`
        },
        {
            targets: 4,
            // createdCell: function (td) {
            //     $(td).addClass('text-center align-items-center small mx-4 pt-4'); // Thêm lớp CSS "pt-3" vào tất cả các ô td
            // }
            render: function (data, type, row) {
                if (type == "display") {
                    return '<img src="' + data + '"style= "width:150px; height:auto;">';
                }
                return data;
            }
        }
    ]
});


/***VÙNG CHỨA CÁC HÀM XỬ LÝ SỰ KIỆN */
$(document).ready(function () {
    callApiGetListPet();
})

/***Xử lý lọc danh sách pet theo giá */
function filteredPet(){
    console.log("Nhấn nút lọc");
    gDataTable.clear();
    var min = $("#input-min").val();
    var max = $("#input-max").val();
    onPriceFilterClick(min, max);
}

/***Khi nhấn nút thêm, hiển thị modal */
$("#btn-add").on("click", function () {
    $("#modal-add-pet").modal("show");
});

/**Xử lý khi nhấn nút Thêm trên form */
$("#btn-add-pet").on("click", function () {
    onBtnCreatePetClick();
});

/**Xử lý nhấn nút icon EDIT */
$(document).on("click", ".btn-edit", function () {
    onBtnEditClick(this);
});

/**Xử lý khi nhấn icon DELETE, hiện modal xác nhận */
$("#table-pets").on("click", ".btn-delete", function () {
    gPetId = getPetIdFromButton(this);
    $("#modal-confirm-delete").modal("show");
});

/***Gọi API xóa đơn hàng khi nhấn nút xác nhận **/
$("#btn-confirm-delete").on("click", function () {
    callApiDelete();
});

/***hàm được khi khi nhấn nút Thêm  */
function onBtnCreatePetClick() {
    var pet = {
        type: "",
        name: "",
        description: "",
        imageUrl: "",
        price: 0,
        promotionPrice: 0,
        discount: 0
    }
    getInputAdd(pet);
    callApiAdd(pet);
}

/***Xử lý khi nhấn nút Cập nhật */
function onBtnUpdateClick() {
    var pet = {
        type: "",
        name: "",
        description: "",
        imageUrl: "",
        price: 0,
        promotionPrice: 0,
        discount: 0
    }
    getInputEdit(pet);
    callApiUpdate(pet);
}

/***XỬ lý khi nhấn nút edit */
function onBtnEditClick(btnEdit) {
    gPetId = getPetIdFromButton(btnEdit);
    callApiGetPetById(gPetId);
    $("#modal-edit-pet").modal("show");//hiển thị modal edit
}


/***VÙNG CHỨA CÁC HÀM DÙNG CHUNG */

/***Lấy ds pet từ API */
function callApiGetListPet() {
    $.ajax({
        type: 'GET',
        url: gBASE_URL + '/pets',
        dataType: 'json',
        success: function (paramData) {
            console.log(paramData);
            loadListPetOnTable(paramData.rows);
            var limitPet = paramData.rows.slice(0, 8);
            displayPetOnWeb(limitPet);
        },
        error: function (error) {
            console.log(error);
        }
    });
}

/***GỌI API thêm pet */
function callApiAdd(pet) {
    $.ajax({
        url: gBASE_URL + "/pets",
        type: "POST",
        contentType: gCONTENT_TYPE,
        data: JSON.stringify(pet),
        success: function () {
            handleAddSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/***Gọi API cập nhật */
function callApiUpdate(paramPet) {
    $.ajax({
        url: gBASE_URL + "/pets/" + gPetId,
        type: "PUT",
        contentType: gCONTENT_TYPE,
        data: JSON.stringify(paramPet),
        success: function () {
            handleUpdateSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/**Gọi API xóa thông tin pet */
function callApiDelete() {
    $.ajax({
        url: gBASE_URL + "/pets/" + gPetId,
        type: "DELETE",
        contentType: gCONTENT_TYPE,
        success: function (paramData) {
            // Xử lý front-end
            handleDeleteSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/***lấy chi tiết thông tin của vật nuôi */
function callApiGetPetById(petId) {
    $.ajax({
        url: gBASE_URL + "/pets/" + petId,
        type: "GET",
        dataType: 'json',
        success: function (paramData) {
            showPetDetailToModal(paramData); //hiển thị thông tin user lên form modal
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}

/***Tải danh sách vật nuôi lên data table */
function loadListPetOnTable(pets) {
    gDataTable.clear();
    gDataTable.rows.add(pets);
    gDataTable.draw();
}

/***Lấy Id của sản phẩm từ button */
function getPetIdFromButton(paramButton) {
    var tableRow = $(paramButton).parents("tr");
    var petInfo = gDataTable.row(tableRow).data();
    return petInfo.id;
}

//lấy thông tin nhập vào
function getInputAdd(pet) {
    pet.name = $("#input-add-name").val().trim();
    pet.type = $("#select-add-type").val();
    pet.price = $("#input-add-price").val();
    pet.discount = $("#input-add-discount").val();
    pet.imageUrl = $("#input-add-url").val().trim();
    pet.description = $("#input-add-description").val().trim();
    pet.promotionPrice = $("#input-add-promotion-price").val();
}

/**lấy dữ liệu nhập vào trên form sửa */
function getInputEdit(pet) {
    pet.name = $("#input-edit-name").val().trim();
    pet.type = $("#select-edit-type").val();
    pet.price = $("#input-edit-price").val();
    pet.discount = $("#input-edit-discount").val();
    pet.description = $("#input-edit-description").val().trim();
    pet.imageUrl = $("#input-edit-url").val().trim();
    pet.promotionPrice = $("#input-edit-promotion-price").val();
}

//kiểm tra dữ liệu nhập vào form filter
function checkInputPriceFilter(min, max) {
    if (max == "") {
        alert("Chưa nhập giá cao nhất");
        return false;
    }
    if (min == "") {
        alert("Chưa nhập giá thấp nhất");
        return false;
    }
    return true;
}

/**hiển thị chi tiết lên modal */
function showPetDetailToModal(paramPet) {
    $("#input-edit-name").val(paramPet.name);
    $("#select-edit-type").val(paramPet.type);
    $("#input-edit-price").val(paramPet.price);
    $("#input-edit-discount").val(paramPet.discount);
    $("#input-edit-promotion-price").val(paramPet.promotionPrice);
    $("#input-edit-description").val(paramPet.description);
    $("#input-edit-url").val(paramPet.imageUrl);
}

/***Xử lý front end khi cập nhật thành công */
function handleUpdateSuccess() {
    $("#modal-edit-pet").modal("hide");

    //hiện modal thông báo thành công
    $("#modal-update-success").modal("show");
    // Ẩn modal sau 1.5 giây
    setTimeout(function () {
        $("#modal-update-success").modal("hide");
    }, 1500);
    clearInput(); //xóa trắng form
    callApiGetListPet();
}

function clearInput() {
    $("#input-edit-name").val("");
    $("#select-edit-type").val("");
    $("#input-edit-price").val("");
    $("#input-edit-discount").val("");
    $("#input-edit-description").val("");
    $("#input-edit-image").attr("src", "");
}

/**Xử lý front end khi xóa thành công */
function handleDeleteSuccess() {
    $("#modal-confirm-delete").modal("hide");
    $("#modal-delete-success").modal("show");
    //hiện modal thông báo xóa thành công trong 1.5s
    setTimeout(function () {
        $("#modal-delete-success").modal("hide");
    }, 1500);
    callApiGetListPet();
}

function handleAddSuccess() {
    $("#modal-add-pet").modal("hide");
    $("#modal-add-success").modal("show");
    //hiện modal thông báo thêm thành công trong 1.5s
    setTimeout(function () {
        $("#modal-add-success").modal("hide");
    }, 1500);
    callApiGetListPet();
}

/***Lọc trang với giá cao nhất và thấp nhất được nhập***/
function onPriceFilterClick(min, max) {
    const queryParams = new URLSearchParams();
    queryParams.append('priceMin', min);
    queryParams.append('priceMax', max);
    // Gửi yêu cầu AJAX với các tham số đã được lọc
    callApiFilter(queryParams);
}

//API lọc danh sách theo giá
function callApiFilter(queryParams) {
    $.ajax({
        type: 'GET',
        url: gBASE_URL + "/pets?" + queryParams.toString(),
        dataType: 'json',
        success: function (paramData) {
            loadListPetOnTable(paramData.rows);
        },
        error: function (paramError) {
            console.log(paramError);
        }
    });
}

/**hiển thị lên web */
function displayPetOnWeb(pets) {
    pets.forEach(function (pet) {
        $("#pet-container").append(`
        <div class="col-lg-3 col-md-6 col-sm-12">
            <div class="card shadow  equal-height border-0 mb-4">
                <img class="card-img-top img-fluid" src="${pet.imageUrl}">
                <div class="card-body text-center">
                    <h4 class="card-title my-1">${pet.name}</h4>
                    <p class="card-text text-muted my-1">${pet.description}</p>
                    <div class="row d-flex justify-content-center align-items-center">
                        <div class="col-lg-8 text-muted">
                            <div class="row d-flex justify-content-center align-items-center price">
                                <div class="col-6 d-flex ">$${pet.promotionPrice}</div>
                                <del class="col-6 text-decoration-line-through ">$${pet.price}</del>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        `);
    });
}
